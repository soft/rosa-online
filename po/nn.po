# Translation of mdkonline to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2004, 2005, 2007, 2008, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: mdkonline\n"
"POT-Creation-Date: 2015-02-16 12:07+0300\n"
"PO-Revision-Date: 2010-06-08 07:54+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. -PO: here %s will be replaced by the local time (eg: "Will check updates at 14:03:50"
#: ../mdkapplet:72
#, c-format
msgid "Will check updates at %s"
msgstr "Vil sjå etter oppdateringar %s"

#: ../mdkapplet:79
#, c-format
msgid "Your system is up-to-date"
msgstr "Systemet ditt er oppdatert."

#: ../mdkapplet:84
#, fuzzy, c-format
msgid ""
"Service configuration problem. Please report bug at http://bugs.rosalinux.ru"
msgstr ""
"Feil ved oppsett av tenesta. Kontroller loggane og kontakt "
"support@mandrivaonline.com."

#: ../mdkapplet:90
#, c-format
msgid "Please wait, finding available packages..."
msgstr "Ser etter tilgjengelege pakkar. Vent litt …"

#: ../mdkapplet:95
#, c-format
msgid "New updates are available for your system"
msgstr "Det finst nye oppdateringar for systemet ditt."

#: ../mdkapplet:101
#, c-format
msgid "A new version of ROSA Linux distribution has been released"
msgstr "Ein ny versjon av ROSA Linux er no ute."

#: ../mdkapplet:106
#, c-format
msgid "Network is down. Please configure your network"
msgstr ""
"Nettverket er ikkje tilgjengeleg. Kontroller at nettverksoppsettet fungerer."

#: ../mdkapplet:112
#, c-format
msgid "Service is not activated. Please click on \"Online Website\""
msgstr "Tenesta er ikkje starta. Vel «Heimeside»."

#: ../mdkapplet:117 ../mdkapplet:123
#, c-format
msgid "urpmi database locked"
msgstr "urpmi-databasen er låst."

#: ../mdkapplet:128
#, c-format
msgid ""
"No medium found. You must add some media through 'Software Media Manager'."
msgstr "Fann ingen medium. Du må leggja til medium gjennom «Mediehandtering»."

#: ../mdkapplet:133
#, c-format
msgid ""
"You already have at least one update medium configured, but\n"
"all of them are currently disabled. You should run the Software\n"
"Media Manager to enable at least one (check it in the \"%s\"\n"
"column).\n"
"\n"
"Then, restart \"%s\"."
msgstr ""
"Du har allereie minst eitt oppdateringsmedium, men ingen av\n"
"dei er i bruk. Du bør køyra medieoppsettprogrammet, og der\n"
"velja å bruka minst eitt av media (sjå i «%s»-kolonnen).\n"
"\n"
"Start så %s på nytt."

#: ../mdkapplet:138
#, c-format
msgid "Enabled"
msgstr "I bruk"

#: ../mdkapplet:151
#, c-format
msgid "Error updating media"
msgstr "Feil ved oppdatering av medie"

#: ../mdkapplet:181 ../mdkapplet:677
#, c-format
msgid "Install updates"
msgstr "Installer oppdateringane"

#: ../mdkapplet:182
#, c-format
msgid "Check Updates"
msgstr "Sjå etter oppdateringar"

#: ../mdkapplet:183
#, c-format
msgid "Configure Network"
msgstr "Set opp nettverket"

#: ../mdkapplet:184
#, c-format
msgid "Upgrade the system"
msgstr "Oppgrader systemet"

#: ../mdkapplet:350
#, c-format
msgid "Received SIGHUP (probably an upgrade has finished), restarting applet."
msgstr ""
"Fekk SIGHUP-signal (sannsynlegvis er ei oppgradering ferdig). Startar "
"programmet på nytt."

#: ../mdkapplet:357
#, c-format
msgid "Launching drakconnect\n"
msgstr "Køyrer drakconnect\n"

#: ../mdkapplet:364 ../mdkapplet:412
#, c-format
msgid "New version of ROSA Linux distribution"
msgstr "Ny versjon av ROSA Linux"

#: ../mdkapplet:369
#, c-format
msgid "Browse"
msgstr "Bla gjennom"

#: ../mdkapplet:378
#, c-format
msgid "A new version of ROSA Linux distribution has been released."
msgstr "Ein ny versjon av ROSA Linux er no ute."

#: ../mdkapplet:380
#, c-format
msgid "More info about this new version"
msgstr "Meir informasjon om denne nye versjonen"

#: ../mdkapplet:382
#, c-format
msgid "Do you want to upgrade to the '%s' distribution?"
msgstr "Ønskjer du å oppgradera til «%s»?"

#: ../mdkapplet:384
#, c-format
msgid "Do not ask me next time"
msgstr "Spør ikkje igjen"

#: ../mdkapplet:386
#, c-format
msgid "Download all packages at once"
msgstr "Last ned alle pakkane først"

#: ../mdkapplet:387
#, c-format
msgid "(Warning: You will need quite a lot of free space)"
msgstr "(Åtvaring: Du treng då mykje ledig diskplass.)"

#: ../mdkapplet:393
#, c-format
msgid "Where to download packages:"
msgstr "Lagra pakkane i mappa:"

#: ../mdkapplet:397 ../mdkapplet:437
#, c-format
msgid "Next"
msgstr "Neste"

#: ../mdkapplet:397 ../mdkapplet:437
#, c-format
msgid "Cancel"
msgstr "Avbryt"

#: ../mdkapplet:428
#, c-format
msgid ""
"This upgrade requires high bandwidth network connection (cable, xDSL, ...)  "
"and may take several hours to complete."
msgstr ""
"Denne oppgraderinga krev eit raskt Internett-samband (kabelmodus, xDSL, …), "
"og kan ta fleire timar å fullføra."

#: ../mdkapplet:430
#, c-format
msgid "Estimated download data will be %s"
msgstr "Estimert nedlastingsdata: %s"

#: ../mdkapplet:431
#, c-format
msgid "You should close all other running applications before continuing."
msgstr "Du bør lukka alle andre program som køyrer før du held fram."

#: ../mdkapplet:434
#, c-format
msgid ""
"You should put your laptop on AC and favor ethernet connection over wifi, if "
"available."
msgstr ""
"Du bør kopla den berbare maskina til straumuttaket, og om mogleg bruka "
"kabeltilkopling i staden for trådlaust samband."

#: ../mdkapplet:466
#, fuzzy, c-format
msgid "Launching RosaUpdate\n"
msgstr "Køyrer ROSAUpdate\n"

#: ../mdkapplet:486
#, c-format
msgid "Computing new updates...\n"
msgstr "Granskar nye oppdateringar …\n"

#: ../mdkapplet:573
#, c-format
msgid "System is up-to-date\n"
msgstr "Systemet er oppdatert\n"

#: ../mdkapplet:603
#, c-format
msgid "Checking Network: seems disabled\n"
msgstr "Kontrollerer nettverk: Ser avslått ut.\n"

#: ../mdkapplet:630
#, c-format
msgid "ROSA Online %s"
msgstr "ROSA Online %s"

#: ../mdkapplet:631
#, c-format
msgid "Copyright (C) %s by ROSA"
msgstr "Copyright © %s ROSA"

#: ../mdkapplet:634
#, c-format
msgid "ROSA Online gives access to ROSA web services."
msgstr "ROSA Online gjev deg tilgang til vevtenestene til ROSA."

#: ../mdkapplet:636
#, c-format
msgid "Online WebSite"
msgstr "Heimeside"

#. -PO: put here name(s) and email(s) of translator(s) (eg: "John Smith <jsmith@nowhere.com>")
#: ../mdkapplet:641
#, c-format
msgid "_: Translator(s) name(s) & email(s)\n"
msgstr "Karl Ove Hufthammer <karl@huftis.org>\n"

#: ../mdkapplet:668
#, c-format
msgid "Warning"
msgstr "Åtvaring"

#: ../mdkapplet:671
#, c-format
msgid "More Information"
msgstr "Meir informasjon"

#: ../mdkapplet:679
#, c-format
msgid "Add media"
msgstr "Legg til medium"

#: ../mdkapplet:694
#, c-format
msgid "About..."
msgstr "Om …"

#: ../mdkapplet:696 ../mdkapplet-config:67
#, c-format
msgid "Updates Configuration"
msgstr "Oppdateringsoppsett"

#: ../mdkapplet:698
#, c-format
msgid "Always launch on startup"
msgstr "Køyr ved oppstart"

#: ../mdkapplet:700
#, c-format
msgid "Quit"
msgstr "Avslutt"

#: ../mdkapplet-config:43
#, c-format
msgid "Adding an additional package medium"
msgstr "Legg til nytt pakkemedium"

#: ../mdkapplet-config:68
#, c-format
msgid "Here you can configure the updates applet"
msgstr "Her kan du setja opp oppdateringsprogrammet"

#: ../mdkapplet-config:70
#, c-format
msgid "Update frequency (hours)"
msgstr "Oppdateringsfrekvens (timar)"

#: ../mdkapplet-config:79
#, c-format
msgid "First check delay (minutes)"
msgstr "Pause før første sjekk (minutt)"

#: ../mdkonline.pm:147 ../mdkonline.pm:161
#, c-format
msgid "ROSA Free"
msgstr "ROSA Free"

#: ../mdkonline.pm:148
#, c-format
msgid "ROSA Mini"
msgstr "ROSA Mini"

#: ../mdkonline.pm:149
#, c-format
msgid "ROSA One"
msgstr "ROSA One"

#: ../mdkonline.pm:162
#, c-format
msgid "The 100%% Open Source distribution freely available."
msgstr "Distribusjonen med 100%% fri programvare."

#: ../mdkonline.pm:176
#, c-format
msgid "Distribution Upgrade"
msgstr "Oppgradering av distribusjon"

#: ../mdkupdate:58
#, c-format
msgid ""
"mdkupdate version %s\n"
"Copyright (C) %s ROSA.\n"
"This is free software and may be redistributed under the terms of the GNU "
"GPL.\n"
"\n"
"usage:\n"
msgstr ""
"mdkupdate – versjon %s\n"
"Copyright © %s ROSA.\n"
"Dette er fri programvare, og kan distribuerast under vilkåra i GNU GPL-"
"lisensen.\n"
"\n"
"Bruk:\n"

#: ../mdkupdate:63
#, c-format
msgid "  --help\t\t- print this help message.\n"
msgstr "  --help         – Vis denne hjelpeteksten.\n"

#: ../mdkupdate:64
#, c-format
msgid "  --auto\t\t- ROSA Update launched automatically.\n"
msgstr "  --auto         – ROSA Update køyrd automatisk.\n"

#: ../mdkupdate:65
#, c-format
msgid "  --mnf\t\t\t- launch mnf specific scripts.\n"
msgstr "  --mnf          – Køyr mnf-spesifikke skript.\n"

#: ../mdkupdate:66
#, c-format
msgid "  --noX\t\t\t- text mode version of ROSA Update.\n"
msgstr "  --noX          – Tekstutgåva av ROSA Update.\n"

#: ../mdkupdate:67
#, c-format
msgid "  --debug\t\t\t- log what is done\n"
msgstr "  --debug\t\t\t– Logg over kva som vert gjort.\n"

#: ../mdkupdate:97
#, c-format
msgid "Unable to update packages from update_source medium.\n"
msgstr "Klarte ikkje oppdatera pakkar frå oppdateringsmedium.\n"

#, fuzzy
#~ msgid "Check for newer ROSA releases"
#~ msgstr "Sjå etter ei ny utgåve av «%s»"

#~ msgid ""
#~ "ROSA Powerpack brings you the best of Linux experience for desktop: "
#~ "stability and efficiency of open source solutions together with exclusive "
#~ "softwares and ROSA official support."
#~ msgstr ""
#~ "ROSA Powerpack gjev deg den beste Linux-opplevinga – stabilitet og "
#~ "effektivitet, med løysingar baserte på fri programvare saman med "
#~ "eksklusive ROSA-program, og med offisiell kundestøtte."

#~ msgid "ROSA Linux Features"
#~ msgstr "Funksjonar i ROSA Linux"

#~ msgid "Online subscription"
#~ msgstr "Abonnement"

#~ msgid "More information on your user account"
#~ msgstr "Meir informasjon om brukarkontoen din"

#~ msgid "Your email"
#~ msgstr "E-postadresse"

#~ msgid "Your password"
#~ msgstr "Passord"

#~ msgid "Forgotten password"
#~ msgstr "Gløymt passord"

#~ msgid "Error"
#~ msgstr "Feil"

#~ msgid "Password and email cannot be empty."
#~ msgstr "Passord- og e-postfelta kan ikkje stå tomme."

#~ msgid "Close"
#~ msgstr "Lukk"

#~ msgid "Choose your upgrade version"
#~ msgstr "Vel oppgraderingsversjon"

#~ msgid "Your Powerpack access has ended"
#~ msgstr "Du har ikkje lenger tilgang til Powerpack"

#~ msgid "%s is now available, you can upgrade to:"
#~ msgstr "%s er no tilgjengeleg. Du kan oppgradera til:"

#~ msgid "ROSA Flash"
#~ msgstr "ROSA Flash"

#~ msgid "ROSA PowerPack"
#~ msgstr "ROSA PowerPack"

# skip-rule: server
#~ msgid "ROSA Enterprise Server"
#~ msgstr "ROSA Enterprise Server"

#~ msgid ""
#~ "The ROSA Linux distribution with even more softwares and official support."
#~ msgstr ""
#~ "ROSA Linux-distribusjonen med endå fleire program, og med kundestøtte."

#~ msgid "An additional package medium is available for your distribution."
#~ msgstr "Eit ekstra pakkemedium er tilgjengeleg for distribusjonen din."

#~ msgid "Release not supported (too old release, or development release)"
#~ msgstr ""
#~ "Utgåva di er ikkje støtta (anten for gammal eller utviklingsutgåve)."

#~ msgid "Add additional package medium"
#~ msgstr "Legg til ekstra pakkemedium"

#~ msgid ""
#~ "Basic maintenance for this distribution has expired. Thanks to your "
#~ "subscription to extended maintenance, your system will be kept up to date "
#~ "until %s"
#~ msgstr ""
#~ "Grunnleggjande vedlikehald for denne distribusjonen er ikkje støtta. "
#~ "Sidan du abonnerer på utvida vedlikehald, får du likevel oppdateringar "
#~ "fram til %s."

#~ msgid ""
#~ "Maintenance for this ROSA Linux version has ended. No more updates will "
#~ "be delivered for this system."
#~ msgstr ""
#~ "Vedlikehaldsplanen for denne utgåva av ROSA Linux er no over. Du vil "
#~ "ikkje få nokon nye oppdateringar framover."

#~ msgid "In order to keep your system secure, you can:"
#~ msgstr "For å halda systemet sikkert, kan du:"

#~ msgid "ROSA Linux"
#~ msgstr "ROSA Linux"

#~ msgid "You should get extended maintenance."
#~ msgstr "Du bør skaffa utvida vedlikehald."

#~ msgid ""
#~ "You should either get extended maintenance or upgrade to a newer version "
#~ "of the %s distribution."
#~ msgstr ""
#~ "Du bør anten skaffa utvida vedlikehald, eller oppgradera til ein ny "
#~ "versjon av %s."

#~ msgid "You should upgrade to a newer version of the %s distribution."
#~ msgstr "Du bør oppgradera til ein ny versjon av %s."

#~ msgid "Your distribution is no longer supported"
#~ msgstr "Distribusjonen er ikkje lenger støtta"

#~ msgid "Extended Maintenance"
#~ msgstr "Utvida vedlikehald"

#~ msgid ""
#~ "Purchase a maintenance extension for this version (%s) and keep it "
#~ "running until %s."
#~ msgstr ""
#~ "Kjøp ei vedlikehaldsutviding for denne versjonen (%s) for å få "
#~ "oppdateringar fram til %s."

#~ msgid "New medium available"
#~ msgstr "Nytt medium tilgjengeleg"

#~ msgid ""
#~ "You use '%s' distribution and therefore have privileged access to "
#~ "additional software."
#~ msgstr ""
#~ "Du brukar distribusjonen «%s», og har derfor tilgang til meir programvare."

#~ msgid "Do you want to install this additional software repository?"
#~ msgstr "Vil du leggja til dette nye programvarelageret?"

#~ msgid "Supported products are %s, '%s' is not on the list.\n"
#~ msgstr "Dei støtta produkta er %s. «%s» er ikkje støtta.\n"

#~ msgid "Please fill in your account ID to add an additional package medium"
#~ msgstr ""
#~ "Skriv inn e-postadressa og passord til kontoen din for å leggja til det "
#~ "nye pakkemediet."

#~ msgid "Failure while retrieving distributions list:"
#~ msgstr "Feil ved henting av distribusjonslista:"

#~ msgid ""
#~ "ROSA provides 12 months of desktop updates (until %s) and 18 months of "
#~ "base updates (up to the %s) for distributions."
#~ msgstr ""
#~ "ROSA tilbyr 12 månader med generelle oppdateringar (fram til %s), samt 18 "
#~ "månader med grunnoppdateringar (fram til %s)."

#~ msgid ""
#~ "Extended maintenance is now available to get 18 months of additional "
#~ "updates (until %s)."
#~ msgstr ""
#~ "Utvida vedlikehald er no tilgjengeleg, og gjev 18 månader meir med "
#~ "oppdateringar (fram til %s)."

#~ msgid "You can subscribe <b>right now</b> to get extended maintenance:"
#~ msgstr "Du kan starta eit abonnement på utvida vedlikehald <b>nett no</b>:"

#~ msgid "Lifetime policy"
#~ msgstr "Oppdateringsperiodar"

#~ msgid "An error occurred"
#~ msgstr "Det oppstod ein feil"

#~ msgid "Your ROSA account does not have %s download subscription enabled."
#~ msgstr "ROSA-kontoen din har ikkje eit nedlastingsabonnement for %s."

#~ msgid "An error occurred while adding medium"
#~ msgstr "Klarte ikkje leggja til nytt medium."

#~ msgid "Successfully added media!"
#~ msgstr "La til media."

#~ msgid "Successfully added media %s."
#~ msgstr "La til mediet «%s»."

#~ msgid "Ok"
#~ msgstr "OK"

#~ msgid "Check for missing \"%s\" media"
#~ msgstr "Sjå etter «%s»-medium som manglar"

#~ msgid "Restricted"
#~ msgstr "Avgrensa"

#~ msgid "Enterprise"
#~ msgstr "Bedrift"

#~ msgid ""
#~ "Please fill in your account ID to add an additional package medium once "
#~ "you have subscribed online"
#~ msgstr ""
#~ "Etter at du har starta abonnementet, kan du skriva inn e-postadressa og "
#~ "passord ditt her for å leggja til det nye pakkemediet."

#~ msgid ""
#~ "Your ROSA account does not have Extended Maintenance subscription enabled."
#~ msgstr "ROSA-kontoen din har ikkje eit abonnement på utvida vedlikehald."

#~ msgid "Would you like Powerpack?"
#~ msgstr "Ønskjer du Powerpack?"

#~ msgid ""
#~ "Since you don't have Powerpack rights you may visit mandriva store now "
#~ "and get Powerpack subscription."
#~ msgstr ""
#~ "Sidan du ikkje har Powerpack, kan du besøkja ROSA-butikken no, og skaffa "
#~ "deg eit Powerpack-abonnement."

#~ msgid "Get Powerpack subscription!"
#~ msgstr "Skaff Powerpack-abonnement"

#~ msgid ""
#~ "Continue to use your new Powerpack account information to upgrade, or "
#~ "Cancel and upgrade to the Free Edition."
#~ msgstr ""
#~ "Hald fram med å bruka Powerpack-kontoinformasjonen til å oppgradera, "
#~ "eller trykk «Avbryt» for å oppgradera til Free-utgåva."

#~ msgid "Continue and Authenticate!"
#~ msgstr "Hald og fram, og skriv inn kontoinfo"

#~ msgid "Cancel, upgrade to Free Edition"
#~ msgstr "Avbryt, og oppgrader til Free-utgåva"

#~ msgid ""
#~ "Your ROSA account does not have Powerpack download subscription enabled."
#~ msgstr "ROSA-kontoen din har ikkje eit nedlastingsabonnement for PowePack."

#~ msgid ""
#~ "Your system does not have enough space left in %s for upgrade (%dMB < "
#~ "%dMB)"
#~ msgstr ""
#~ "Systemet ditt har ikkje nok ledig plass att på «%s» for ei oppgradering "
#~ "(%d MB < %d MB)"

#~ msgid "Installation failed"
#~ msgstr "Feil ved installering"

#~ msgid "Installation logs can be found in '%s'"
#~ msgstr "Du finn installasjonsloggar i «%s»"

#~ msgid "Retry"
#~ msgstr "Prøv på nytt"

#~ msgid "Congratulations"
#~ msgstr "Gratulerer"

#~ msgid "Upgrade to ROSA %s release was successfull."
#~ msgstr "Oppgraderinga til ROSA %s er no fullført."

#~ msgid "You must restart your system."
#~ msgstr "Du må starta maskina på nytt."

#~ msgid "Reboot"
#~ msgstr "Start på nytt"

#~ msgid ""
#~ "Packages database is locked. Please close other applications\n"
#~ "working with packages database (do you have another media\n"
#~ "manager on another desktop, or are you currently installing\n"
#~ "packages as well?)."
#~ msgstr ""
#~ "Pakkedatabasen er låst. Lukk alle andre program\n"
#~ "som brukar pakkedatabasen (har du eit anna\n"
#~ "mediehandsamingsprogram oppe, eller held du på\n"
#~ "å installera andre pakkar no?)."

#~ msgid "Failure when adding medium"
#~ msgstr "Klarte ikkje leggja til nytt medium"

#~ msgid ""
#~ "This ROSA Linux system maintenance has ended. It means it will not "
#~ "receive any new software update."
#~ msgstr ""
#~ "Vedlikehald for dette ROSA Linux-systemet er ikkje lenger støtta. Dette "
#~ "medfører at du i framtida ikkje får nye programvareoppdateringar."

#, fuzzy
#~| msgid "Do you want to upgrade to the '%s' distribution?"
#~ msgid "You are using '%s' distribution."
#~ msgstr "Ønskjer du å oppgradera til «%s»?"

#~ msgid "Do you want to upgrade?"
#~ msgstr "Ønskjer du å oppgradera?"

#~ msgid "Yes"
#~ msgstr "Ja"

#~ msgid "No"
#~ msgstr "Nei"

#~ msgid "ROSA Online seems to be reinstalled, reloading applet ...."
#~ msgstr "ROSA Online er installert på nytt. Startar panelprogrammet om att."

#~ msgid "Checking... Updates are available\n"
#~ msgstr "Kontrollererer … Det finst nye oppdateringar.\n"

#~ msgid "Packages are up to date"
#~ msgstr "Alle pakkane er i nyaste versjon."

#~ msgid "Failed to open urpmi database"
#~ msgstr "Klarte ikkje opna pakkedatabase."

#~ msgid "Connecting to"
#~ msgstr "Koplar til"

#~ msgid "ROSA Linux Updates Applet"
#~ msgstr "Panelprogram for ROSA Linux-oppdateringar"

#~ msgid "Security error"
#~ msgstr "Tryggleiksfeil"

#~ msgid "Generic error (machine already registered)"
#~ msgstr "Generell feil (maskina er alt registrert)"

#~ msgid "Database error"
#~ msgstr "Databasefeil"

#~ msgid ""
#~ "Server Database failed\n"
#~ "Please Try again Later"
#~ msgstr ""
#~ "Feil ved tenardatabase.\n"
#~ "Prøv på nytt seinare."

#~ msgid "Registration error"
#~ msgstr "Registreringsfeil"

#~ msgid "Some parameters are missing"
#~ msgstr "Manglar nokre parametrar"

#~ msgid "Password error"
#~ msgstr "Passordfeil"

#~ msgid "Login error"
#~ msgstr "Feil ved innlogging"

#~ msgid ""
#~ "The email you provided is already in use\n"
#~ "Please enter another one\n"
#~ msgstr ""
#~ "E-postadressa er allereie i bruk.\n"
#~ "Skriv inn ei anna adresse.\n"

#~ msgid "The email you provided is invalid or forbidden"
#~ msgstr "E-postadressa er ugyldig eller forboden."

#~ msgid ""
#~ "Email address box is empty\n"
#~ "Please provide one"
#~ msgstr "Du har gløymt å fylla ut e-postadressa."

#~ msgid "Restriction Error"
#~ msgstr "Avgrensingsfeil"

#~ msgid "Database access forbidden"
#~ msgstr "Nekta tilgang til database"

#~ msgid "Service error"
#~ msgstr "Tenestefeil"

#~ msgid ""
#~ "ROSA web services are currently unavailable\n"
#~ "Please Try again Later"
#~ msgstr ""
#~ "ROSA-tenestene er ikkje tilgjengelege nett no.\n"
#~ "Prøv på nytt seinare."

#~ msgid "Password mismatch"
#~ msgstr "Passorda er ikkje like."

#~ msgid ""
#~ "ROSA web services are under maintenance\n"
#~ "Please Try again Later"
#~ msgstr ""
#~ "ROSA-tenestene er under omlegging.\n"
#~ "Prøv på nytt seinare."

#~ msgid "User Forbidden"
#~ msgstr "Brukar nekta tilgang"

#~ msgid "User account forbidden by ROSA web services"
#~ msgstr "Brukarkontoen er nekta tilgang til ROSA-tenestene"

#~ msgid "Connection error"
#~ msgstr "Sambandsfeil"

#~ msgid "ROSA web services not reachable"
#~ msgstr "Får ikkje kontakt med ROSA-tenestene"

#~ msgid ""
#~ "  --bundle file.bundle\t- parse and install package from .bundle metainfo "
#~ "file.\n"
#~ msgstr ""
#~ "  --bundle file.bundle\t– Installer pakkar frå ei .bundle-metainfofil.\n"

#~ msgid ""
#~ "You first need to install the system on your harddrive with the 'Live "
#~ "Install' wizard."
#~ msgstr ""
#~ "Du må først installera systemet på harddisken med «Live-install»-"
#~ "vegvisaren."

#~ msgid "Please wait"
#~ msgstr "Vent litt"

#~ msgid "Preparing..."
#~ msgstr "Førebur …"

#~ msgid ""
#~ "Failed to authenticate to the bundle server:\n"
#~ "\n"
#~ "%s"
#~ msgstr ""
#~ "Godkjenningsfeil på programsamlingtenaren:\n"
#~ "\n"
#~ "%s"

#~ msgid ""
#~ "The version of the ROSA Online client is too old.\n"
#~ "\n"
#~ "You need to update to a newer version. You can get a new one from http://"
#~ "start.mandriva.com"
#~ msgstr ""
#~ "Denne versjonen av ROSA Online er for gammal.\n"
#~ "\n"
#~ "Du må oppdatera til ein nyare versjon. Du kan lasta ned ein ny versjon "
#~ "frå http://start.mandriva.com/."

#~ msgid "This bundle is not well formated. Aborting."
#~ msgstr "Programsamlinga er ugyldig. Avbryt."

#~ msgid "Installing packages ...\n"
#~ msgstr "Installerer pakkar …\n"
